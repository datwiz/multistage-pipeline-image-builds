#------------------------------------------------------------------------------
# setup the base container environment for poetry managed images
# default to building image with pinned package versions unless passed
# a build arg of '--build-arg PYTHON_PKG_VERSIONS=refresh'
#------------------------------------------------------------------------------
ARG PYTHON_PKG_VERSIONS=pinned
FROM python:3.8 as base-pre-pkg

RUN install -d /src && \
    pip install --no-cache-dir poetry==1.1.13 && \
    poetry config virtualenvs.create false
WORKDIR /src

#------------------------------------------------------------------------------
# build using package version constraints defined in pyproject.toml
#------------------------------------------------------------------------------
FROM base-pre-pkg as python-pkg-refresh
COPY pyproject.toml poetry.lock /src/
RUN poetry update && \
    poetry install 


#------------------------------------------------------------------------------
# build using pinned python package versions
#------------------------------------------------------------------------------
FROM base-pre-pkg as python-pkg-pinned
COPY pyproject.toml poetry.lock /src/
RUN poetry install 


#------------------------------------------------------------------------------
# post python package install steps
#------------------------------------------------------------------------------
# hadolint ignore=DL3006
FROM python-pkg-${PYTHON_PKG_VERSIONS} as base-post-pkg


#------------------------------------------------------------------------------
# smoke tests for the image
#------------------------------------------------------------------------------
FROM base-post-pkg as smoke-test
WORKDIR /src
COPY tests/ ./tests
RUN poetry --version && \
    python ./tests/module_smoke_test.py


#------------------------------------------------------------------------------
# set final default image target
#------------------------------------------------------------------------------
FROM base-post-pkg as target-image