import pandas as pd

def package_ver_check(module_version, expected_version) -> None:
    assert module_version.startswith(expected_version)

package_ver_check(pd.__version__, '1.4')
